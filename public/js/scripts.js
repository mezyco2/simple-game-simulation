var damage_hit_value =   document.getElementById("damage_hit");
var damage_hit_obj = document.getElementsByClassName("damage-hit");

var who_start = document.getElementById("who_start").value;
var btn_fight = document.getElementsByClassName("btn-fight");

var message_popup = document.getElementsByClassName("message_popup");
var message_popup_skills = document.getElementsByClassName("message_popup_skills");

var hero_health = document.getElementById('hero_health');
var beast_health = document.getElementById('beast_health');
var hero_health_bar = document.getElementById('hero_health_bar');
var beast_health_bar = document.getElementById('beast_health_bar');

var beast_move = document.getElementsByClassName('beast_move');
var hero_move = document.getElementsByClassName('hero_move');

var usedSkills = "";
var damage = '';
hideDamageHit();

function startHeroMove(){

    hero_move[0].classList.add('move-to-right');
}
function startBeastMove(){

    beast_move[0].classList.add('move-to-left');
}
function hideDamageHit(){
    damage_hit_obj[0].style.display = "none";
}
function showDamageHit(val, side){
    setTimeout(function(){
        damage_hit_obj[0].classList.remove("left");
        damage_hit_obj[0].classList.remove("right");
        damage_hit_obj[0].classList.add(side);
        damage_hit_value.innerHTML = val;
        damage_hit_obj[0].style.display = "block";
        setTimeout(function(){ hideDamageHit(); }, 900);
    }, 1500);
}


function startFight(){
    console.log(btn_fight[0]);

    btn_fight[0].style.display = 'none';
    message_popup[0].style.display = "block";
    setTimeout(function(){
        message_popup[0].style.display = "none";


        fightRounds();
        // curl to knew the damage and next move




    }, 1000);
}
function fightRounds(){
    var request_url = base_url + '/start';
    var response = httpGet(request_url);
    var obj_response = JSON.parse(response);
    if (obj_response.win != null){
        message_popup[0].innerHTML = obj_response.win;
        message_popup[0].classList.remove('alert-danger');
        message_popup[0].classList.add('alert-success');
        message_popup[0].style.display = 'block';
    }
    if (obj_response.round != null){
        message_popup[0].innerHTML = 'Round ' + obj_response.round;
        message_popup[0].classList.remove('alert-danger');
        message_popup[0].classList.add('alert-primary');
        message_popup[0].style.display = 'block';

        setTimeout(function() {
            message_popup[0].style.display = "none";

            if (who_start == 'left'){
                var attacker = obj_response.hero;
                var defender = obj_response.beast;

                simulateAttack(attacker, defender);

                startHeroMove();
                showDamageHit(damage, who_start);

                setTimeout(function() {
                    if (defender.stats.health < 0){
                        defender.stats.health = 0;
                    }
                    beast_health.innerHTML = defender.stats.health;
                    beast_health_bar.style.width = defender.stats.health + "%";
                },2000);

                who_start = "right";
                setTimeout(function() {
                    hero_move[0].classList.remove("move-to-right");
                    fightRounds();
                },2500);
            }else{
                var attacker = obj_response.beast;
                var defender = obj_response.hero;

                simulateAttack(attacker, defender);

                startBeastMove();
                showDamageHit(damage, who_start);

                setTimeout(function() {
                    if (defender.stats.health < 0){
                        defender.stats.health = 0;
                    }
                    hero_health.innerHTML = defender.stats.health;
                    hero_health_bar.style.width = defender.stats.health + "%";
                },2000);

                who_start = "left";
                setTimeout(function() {
                    beast_move[0].classList.remove("move-to-left");
                    fightRounds();
                },2500);
            }
        },2000);
    }

    console.log(obj_response);

}
function simulateAttack(attacker, defender){
    damage = '- '+ attacker.damage;
    if (defender.has_luck === true){
        damage = 'miss';
    }
    usedSkills = "";
    if (attacker.use_skills != null){
        usedSkills += attacker.name + " use the ";
        attacker.use_skills.forEach(showUsedSkills);
        message_popup_skills[0].innerHTML = usedSkills;
        message_popup_skills[0].style.display = 'block';
        setTimeout(function() {
            message_popup_skills[0].style.display = "none";
        },1000);
    }
    if (defender.use_skills != null){
        usedSkills += defender.name + " use the ";
        defender.use_skills.forEach(showUsedSkills);
        message_popup_skills[0].innerHTML = usedSkills;
        message_popup_skills[0].style.display = 'block';
        setTimeout(function() {
            message_popup_skills[0].style.display = "none";
        },1000);
    }
}

function showUsedSkills(obj , index){
    usedSkills += obj.name;
}

function httpGet(theUrl)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
    xmlHttp.send( null );
    return xmlHttp.responseText;
}