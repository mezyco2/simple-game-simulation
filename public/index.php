<?php
require_once dirname(dirname(__FILE__)). '/app/v1/configuration/config.php';
require_once CONFIG_DIR . '/autoloader.php';
\v1\configuration\autoloader::init();
require_once CONFIG_DIR . '/routes.php';