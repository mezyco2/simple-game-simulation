# Simple Game Simulation

Is a simple game in which a hero and a beast fight in the forest

## Installation

You need to upload the database named game.sql.gz to your project.
After the database is set, put the downloaded folder in your apache path. 

If you use linux:
```bash
/var/www/html/game
```
If you use windows:

```windows
C:\xampp\htdocs\game
```

## Usage

To start the game you need to enter the address:
```bash
http://localhost/game
```
To see the game in console mode enter the  address:
```bash
http://localhost/game/console
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## Live URL
- [Game](https://game.sharkdev.ro/)
- [Game Console](https://game.sharkdev.ro/console)

## BUGS FOUND
- When damage is negativ, the defender health is incraced with the damage (health = health -(-damage)
- Health is not rounded, it can appear like 78,3333333333%