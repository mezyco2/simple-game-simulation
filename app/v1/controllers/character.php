<?php
namespace v1\controllers;

use v1\models\character2Skillmodel;
use v1\models\character2Statsmodel;
use v1\models\charactersmodel;
use v1\models\skillsmodel;
use v1\models\statsmodel;
use v1\models\TempModel;

class Character extends basecontroller
{
    private static $instance = NULL;
    private $_charatersModel;
    private $_skillsModel;
    private $_statsModel;
    private $_character2statsModel;
    private $_character2skillModel;

    public function __construct()
    {
        $this->_charatersModel = new charactersmodel();
        $this->_skillsModel = new skillsmodel();
        $this->_statsModel = new statsmodel();
        $this->_character2skillModel = new character2Skillmodel();
        $this->_character2statsModel = new character2Statsmodel();
    }


    public static function getInstace(){
        if (self::$instance == NULL){
            self::$instance = new Character();
        }
        return self::$instance;
    }

    public function init($console = false){
        unset($_SESSION['characters']);
        if (isset($_SESSION['characters'])){
            $characters = json_decode($_SESSION['characters'],true);
        }else{
            $characters['hero'] = $this->_charatersModel->getOneRandomWhere(['type' => 'hero']);
            $characters['beast'] = $this->_charatersModel->getOneRandomWhere(['type'=> 'beast']);

            $characters['hero']['stats'] = $this->_character2statsModel->getCharacterStats($characters['hero']['id']);
            $characters['beast']['stats'] = $this->_character2statsModel->getCharacterStats( $characters['beast']['id']);

            $characters['hero']['skills'] = $this->_character2skillModel->getCharacterSkills($characters['hero']['id']);
            $characters['beast']['skills'] = $this->_character2skillModel->getCharacterSkills( $characters['beast']['id']);

            if ($characters['hero']['stats']['speed'] > $characters['beast']['stats']['speed']){
                $characters['hero']['start'] = 1;
            }elseif($characters['hero']['stats']['speed'] < $characters['beast']['stats']['speed']){
                $characters['beast']['start'] = 1;
            }else{
                if ($characters['hero']['stats']['luck'] >= $characters['beast']['stats']['luck']){
                    $characters['hero']['start'] = 1;
                }else{
                    $characters['beast']['start'] = 1;
                }
            }

            $_SESSION['characters'] = json_encode($characters);
            $_SESSION['round'] = 0;
        }
        if (!$console){
            require_once (VIEW_DIR. "/index.php");
        }

    }

    public function start($console = false){
        if(!isset($_SESSION['characters'])){
            $characters = $this->init();
        }else{
            $characters = json_decode($_SESSION['characters'],true);
        }
        if($_SESSION['round'] > 20 ){
            if ($console){
                $characters['win'] =  "The match has end with no winner" ;
                return $characters;
            }
           echo json_encode(['win' => "The match has end with no winner"]);
        }
        if ($characters['hero']['stats']['health'] <= 0){
            if ($console){
                $characters['win'] =  'The '.$characters['beast']['name'].' wins the battle' ;
                return $characters;
            }
           echo json_encode(['win' => 'The '.$characters['beast']['name'].' wins the battle']);die;
        }
        if ($characters['beast']['stats']['health'] <= 0){
            if ($console){
                $characters['win'] = 'The '.$characters['hero']['name'].' wins the battle' ;
                return $characters;
            }
           echo json_encode(['win' => 'The '.$characters['hero']['name'].' wins the battle']);die;
        }
        if (isset($characters['hero']['start'])){
            // hero start
            return $this->attack($characters['hero'], $characters['beast'], $console);
        }else{
            // beast start
            return $this->attack($characters['beast'], $characters['hero'], $console);
        }
    }

    private function attack($attaker, $defender, $console = false){
        $result = [];

        unset($attaker['use_skills']);
        unset($defender['use_skills']);
        unset($attaker['damage']);
        unset($defender['damage']);
        unset($attaker['has_luck']);
        unset($defender['has_luck']);
        foreach($attaker['skills'] as $skill){
            if (!empty($skill) && $skill['type'] == 'attack'){
                if ($this->randomPercent($skill['chance'])){
                    $attaker['use_skills'][] = $skill;
                }
            }
        }

        foreach($defender['skills'] as $skill){
            if (!empty($skill) && $skill['type'] == 'defence'){
                if ($this->randomPercent($skill['chance'])){
                    $defender['use_skills'][] = $skill;
                }
            }
        }

        $attaker['damage'] = $attaker['stats']['strength'] - $defender['stats']['defence'];
        if (isset($attaker['use_skills'])){
            foreach ($attaker['use_skills'] as $skill){
                if ($skill['damage_decrease'] != null){
                    $attaker['damage'] = (($skill['damage_increase'] * $attaker['damage']) / 100) + $attaker['damage'];
                }
                if ($skill['strike_times'] != null){
                    $attaker['damage'] = $attaker['damage'] * $skill['strike_times'];
                }
            }
        }
        if (isset($defender['use_skills'])){
            foreach ($defender['use_skills'] as $skill){
                $attaker['damage'] = ($skill['damage_decrease'] * $attaker['damage']) / 100;
            }
        }
        $defender['has_luck'] = $this->randomPercent($defender['stats']['luck']);
        $defender['stats']['health'] = $defender['stats']['health'] - (!$defender['has_luck'] * $attaker['damage']);

        $defender['start'] = 1;
        unset ($attaker['start']);

        $characters[$attaker['type']] = $attaker;
        $characters[$defender['type']] = $defender;
//        echo "ROUND ". $_SESSION['round'] . "<hr>";
//        var_export($characters);

        $_SESSION['characters'] = json_encode($characters);
        $_SESSION['round'] ++;
        $characters['round'] = $_SESSION['round'];
        if ($console){
            return $characters;
        }
        echo json_encode($characters);die;
    }

    public function console(){
        if ($_SESSION['round'] > 0){
            $this->init(true); // initialize the game
        }
        $this->printConsole();



    }
    private function printConsole(){
        $characters = $this->start(true);
        if (isset($characters['win'])){
            echo $characters['win'];die;
        }else{
            echo "ROUND " . $characters['round'] . '<br>';
            if (isset($characters['hero']['start'])){
                $this->printResult($characters['beast'],$characters['hero']);
            }else{
                $this->printResult($characters['hero'],$characters['beast']);
            }
            echo "<hr>";
            $this->printConsole();
        }
    }
    private function printResult($attaker, $defender){
        echo $defender['name'] .' was attacked by '. $attaker['name']. '<br>';
        if ($defender['has_luck']){
            echo $defender['name'] .' was lucky because the '. $attaker['name']. ' miss the attack <br>';
        }else{
            if (isset($defender['use_skills'])){
                echo  $defender['name'] .' use the ';
                foreach ($defender['use_skills'] as $skill){
                    echo $skill['name']. ' skill, ';
                }
            }
            if (isset($attaker['use_skills'])){
                echo  $attaker['name'] .' use the ';
                foreach ($attaker['use_skills'] as $skill){
                    echo $skill['name']. ' skill, ';
                }
            }
            echo $defender['name'] .' lose '.  $attaker['damage'] .' points of his life and remains with ';
            if ( $defender['stats']['health'] > 0 ) {
                echo $defender['stats']['health'];
            } else{
                echo 0;
            }
            echo ' points <br>' ;
        }
    }
}