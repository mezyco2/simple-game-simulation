<!DOCTYPE html>
<html lang="en">
<head>
    <title>Simple Game</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script>
        var base_url = '<?php echo BASE_URL ?>';
    </script>
    <link type="text/css" rel="stylesheet" href="<?php echo BASE_URL.'/css/main.css'?>">
</head>
    <body>

        <div class="jungle">
            <button type="button" class="btn btn-danger btn-lg btn-fight" onclick="startFight()">Fight</button>

            <div class="message_popup alert alert-danger" style="display: none;" ><?php if(isset($characters['hero']['start'])){echo $characters['hero']['name'];}else{echo $characters['beast']['name'];}?> hit first!</div>
            <div class="message_popup_skills alert alert-info" style="display: none;"> </div>

            <div class="damage-hit" style="display:none;"><span id="damage_hit">- 30</span><img src="<?= BASE_URL. '/images/heart.png'; ?>"></div>
            <input type="hidden" id="who_start" value="<?php if(isset($characters['hero']['start'])){echo "left";}else{echo "right";}?>">

            <div class="box hero">
                <div class="stats p-4">
                    <h5 class="health"><img src="<?= BASE_URL. '/images/heart.png'; ?>"> Health <span id="hero_health"><?php echo $characters['hero']['stats']['health']; ?></span> %</h5>
                    <div class="progress" >
                        <div class="progress-bar bg-success" id="hero_health_bar" style="width:<?php echo $characters['hero']['stats']['health']; ?>%"></div>
                    </div>
                    <div class="stats strength">
                        <img src="<?= BASE_URL. '/images/strength.png'; ?>">
                        <?php echo $characters['hero']['stats']['strength']; ?>
                    </div>
                    <div class="stats defense">
                        <img src="<?= BASE_URL. '/images/defense.png'; ?>">
                        <?php echo $characters['hero']['stats']['defence']; ?>
                    </div>
                    <div class="stats speed">
                        <img src="<?= BASE_URL. '/images/speed.png'; ?>">
                        <?php echo $characters['hero']['stats']['speed']; ?>
                    </div>
                    <div class="stats luck">
                        <img src="<?= BASE_URL. '/images/luck.png'; ?>">
                        <?php echo $characters['hero']['stats']['luck']; ?>
                    </div>
                </div>
                <img class="hero_move" src="<?= BASE_URL. '/images/hero.png'; ?>">
            </div>
            <div class="box beast">
                <div class="stats p-4">
                    <h5 class="health"> Health <span id="beast_health"><?php echo $characters['beast']['stats']['health']; ?></span> %  <img src="<?= BASE_URL. '/images/heart.png'; ?>"></h5>
                    <div class="progress">
                        <div class="progress-bar bg-success" id="beast_health_bar" style="width:<?php echo $characters['beast']['stats']['health']; ?>%"></div>
                    </div>
                    <div class="stats strength">
                        <img src="<?= BASE_URL. '/images/strength.png'; ?>">
                        <?php echo $characters['beast']['stats']['strength']; ?>
                    </div>
                    <div class="stats defense">
                        <img src="<?= BASE_URL. '/images/defense.png'; ?>">
                        <?php echo $characters['beast']['stats']['defence']; ?>
                    </div>
                    <div class="stats speed">
                        <img src="<?= BASE_URL. '/images/speed.png'; ?>">
                        <?php echo $characters['beast']['stats']['speed']; ?>
                    </div>
                    <div class="stats luck">
                        <img src="<?= BASE_URL. '/images/luck.png'; ?>">
                        <?php echo $characters['beast']['stats']['luck']; ?>
                    </div>
                </div>
                <img class="beast_move" src="<?= BASE_URL. '/images/monster.png'; ?>">
            </div>

        </div>
    </body>
    <script type="application/javascript" src="<?php echo BASE_URL.'/js/scripts.js'?>"></script>
</html>



