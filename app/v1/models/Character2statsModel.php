<?php
namespace v1\models;

class character2Statsmodel extends BaseModel {
    protected $tbl = 'character2stats';

    public function getCharacterStats($character_id){
        $query = "SELECT a.name , floor((rand() *(b.max - b.min)) + b.min) as rand FROM character2stats as b  
                  INNER join stats as a on a.id = b.id_stats
                  where id_character = $character_id";
        $result = $this->db->get($query);
        $return = [];
        foreach ($result as $v){
            $return[$v['name']] = $v['rand'];
        }
        return $return;

    }
}
