<?php
namespace v1\models;

use v1\helpers\DataBase;

Class BaseModel{
    public $db = '';

    public function __construct(){
        $this->db = DataBase::getInstance();
    }

    public function getAll($desc = false){
        $query = 'SELECT * FROM '.$this->tbl;
        $query .= $this->db->_order_by('id', $desc);
        return $this->db->get($query);
    }

    public function getAllWhere($where, $type = 'AND', $desc = false){
        $query = 'SELECT * FROM '.$this->tbl .' '. $this->db->_where($where, $type);
        $query .= $this->db->_order_by('id', $desc);
        return $this->db->get($query);
    }

    public function getAllWhereOrder($where, $type = 'AND', $order_item, $desc = false){
        $query = 'SELECT * FROM '.$this->tbl .' '. $this->db->_where($where, $type);
        $query .= $this->db->_order_by($order_item, $desc);
        return $this->db->get($query);
    }

    public function getOneWhere($where, $type = 'AND', $desc = false){
        $query = 'SELECT * FROM '.$this->tbl .' '. $this->db->_where($where, $type);
        $query .= $this->db->_order_by('id', $desc);
        return $this->db->get($query, true);
    }

    public  function getOneRandomWhere($where){
        $query = 'SELECT * FROM '.$this->tbl .' '. $this->db->_where($where, 'AND');
        $query .= 'ORDER BY RAND()';
        return $this->db->get($query, true);
    }

    public function getOneWhereOrder($where, $type = 'AND', $order_item, $desc = false){
        $query = 'SELECT * FROM '.$this->tbl .' '. $this->db->_where($where, $type);
        $query .= $this->db->_order_by($order_item, $desc);
        return $this->db->get($query, true);
    }

    public function delete($id){
        $query = 'DELETE FROM '.$this->tbl .' '. $this->db->_where(['id' => $id], 'AND');
        return $this->db->run($query);
    }

    public function deleteWhere($where, $type = 'AND'){
        $query = 'DELETE FROM '.$this->tbl .' '. $this->db->_where($where, 'AND');
        return $this->db->run($query);
    }

    public function truncate(){
        $query = 'DELETE FROM '.$this->tbl;
        return $this->db->run($query);
    }

    public function updateItem($data, $id){
        $query = 'UPDATE '.$this->tbl.' '.$this->db->_update($data).' '.$this->db->_where(['id' => $id], 'AND');
        return $this->db->run($query);
    }

    public function updateWhere($data, $where, $type = 'AND' ){
        $query = 'UPDATE '.$this->tbl.' '.$this->db->_update($data).' '.$this->db->_where($where, 'AND');
        return $this->db->run($query);
    }

    public function insert($data){
        $query = 'INSERT INTO '.$this->tbl.' '.$this->db->_insert($data);
        return $this->db->run($query);
    }
}


?>