<?php
namespace v1\models;

class character2Skillmodel extends BaseModel {
    protected $tbl = 'character2skill';


    public function getCharacterSkills($character_id){
        $query = "SELECT a.name, a.type, a.description, a.damage_increase, a.damage_decrease, a.strike_times, b.chance 
                  FROM character2skill as b INNER join skills as a on a.id = b.id_skill where id_character = $character_id";
        return $this->db->get($query);
    }

}
