<?php
$router = new \v1\helpers\router();

$router->get('/', function(){
    return \v1\controllers\Character::getInstace()->init();
});
$router->get('/start', function(){
    return \v1\controllers\Character::getInstace()->start();
});

$router->get('/console', function(){
    return \v1\controllers\Character::getInstace()->console();
});