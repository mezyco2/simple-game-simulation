<?php
namespace v1\configuration;


class autoloader
{
    public static $loader;

    public static function init()
    {
        if (self::$loader == NULL)
            self::$loader = new self();

        return self::$loader;
    }

    public function __construct()
    {
        spl_autoload_register(array($this,'autoload'));
    }

    public function autoload($class)
    {
        $class = str_replace("\\", '/', $class);
        set_include_path(BASE_DIR.'/');
        spl_autoload_extensions(".php");
        spl_autoload($class);
    }
}