<?php
namespace v1\configuration;

define("DEBUG",true); // Disable DEBUG MODE
error_reporting(DEBUG?E_ALL:E_ERROR); // if DEBUG is FALSE the error_reporting is set to E_ERROR
ini_set('display_errors', DEBUG?1:0); // if DEBUG is TRUE the display_error is set to true
/**
* Security Header
*/
header("Strict-Transport-Security:max-age=31536000");
header("X-Frame-Options: SAMEORIGIN");
header("X-Content-Type-Options: nosniff");
header("Cache-Control: max-age=2592000");

session_start(); // start session
define('BASE_URI', '/game');
define("BASE_URL", 'http://' .$_SERVER['HTTP_HOST'].BASE_URI );
define("BASE_DIR", dirname(dirname(dirname(__FILE__))));
define("CONTROLLER_DIR", BASE_DIR.'/v1/controllers');
define("MODEL_DIR", BASE_DIR.'/v1/models');
define("VIEW_DIR", BASE_DIR.'/v1/views');
define("CONFIG_DIR", BASE_DIR.'/v1/configuration');
define("HELPER_DIR", BASE_DIR.'/v1/helpers');

require_once CONFIG_DIR . '/database.php';



