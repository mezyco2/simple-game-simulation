<?php
namespace v1\helpers;

class router
{
    private $supportedHttpMethods = array(
        "GET",
        "POST",
        "PUT",
        "DELETE"
    );

    function __call($name, $args) {
        list($route, $method) = $args;
        if(!in_array(strtoupper($name), $this->supportedHttpMethods)) {
            die('invalid request');
        }
        $this->{strtolower($name)}[$this->formatURI($route)] = $method;
    }

    private function formatURI($route){
        $result = rtrim($route, '/');
        if ($result === '')
        {
            return '/';
        }
        return $result;
    }

    function resolve()
    {
        @$methodDictionary = $this->{strtolower($_SERVER['REQUEST_METHOD'])};

        $uri = str_replace(BASE_URI,'',$_SERVER['REQUEST_URI']);
        $formatedRoute = $this->formatURI($uri);

        @$method = $methodDictionary[$formatedRoute];

        if(is_null($method)){
            die ('404');
        }
        echo call_user_func_array($method, array($this->getBody()));
    }
    function __destruct()
    {
        $this->resolve();
    }


    public function getBody()  {
        if ($_SERVER['REQUEST_METHOD'] === "GET") {
            $result = array();
            foreach ($_GET as $key => $value) {
                $result[$key] = filter_input(INPUT_GET, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
            return $result;
        }
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $result = array();
            foreach ($_POST as $key => $value) {
                $result[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
            $rawData = $this->getRawData();
            if (!empty($rawData)) {
                foreach ($rawData as $key => $value) {
                    $result[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
                }
                $result['isRawData'] = true;
            }
            return $result;
        }
    }

    public function getRawData(){
        $postdata = file_get_contents("php://input");
        $postData = json_decode($postdata,true);
        if ($postData != null){
            return $postData;
        }
        return null;
    }

}