<?php
namespace v1\helpers;

Class Logger {
    
    private static $instance = NULL;
    private $_filename = 'debug';

    public function __construct(){
        
    }
    
    public static function getInstace(){
        if (self::$instance == NULL){
            self::$instance = new Logger();
        }
        return self::$instance;
    }

    /**
     * A simple log to file function (helpfull for debug)
     * @param $data
     */
    public function log($data){
        $day = date('Y-m-d');
        $time = date('Y-m-d H:i:s');
        $file = BASE_DIR.'/v1/logs/'. $this->_filename.'-'.$day.'.txt';
        
        //start write the log
        ob_start();
        echo "====== " . $time ." ======\n";
        print_r($data);
        echo "\n=================================\n";
        
        file_put_contents($file, ob_get_clean(), FILE_APPEND);
    }

    public function setFileName($name){
        $this->_filename = $name;
    }
    
}