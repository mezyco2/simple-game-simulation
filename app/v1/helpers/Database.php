<?php
namespace v1\helpers;
use \PDO;
Class database{

    private $_host;
    private $_username;
    private $_password;
    private $_database;
    private $_conn;
    public $tbl = '';
    public $log;
    public $load;
    
    private static $instance;
    
    // The singleton method
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new DataBase();
        }
        return self::$instance;
    }
    
    public function __construct(){
        $this->log = Logger::getInstace();
        $this->log->setFileName("database");
        $this->connect();
    }

    public function connect(){
        
        if ('' == DB_HOST){
            die('Please set database host'); // no host
        }
        if ('' == DB_USER){
            die('Please set database user'); // no name
        }
        if ('' == DB_PASS){
            die('Please set database password'); // no pass
        }
        if ('' == DB_NAME){
            die('Please set database name'); // no database
        }
        
        $this->_host = DB_HOST;
        $this->_username = DB_USER;
        $this->_password = DB_PASS;
        $this->_database = DB_NAME;
        // try to make the connection :)
        
        try {
            $this->_conn = new PDO("mysql:host=$this->_host;dbname=$this->_database", $this->_username, $this->_password);
            // set the PDO error mode to exception
            $this->_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->_conn->exec("set names utf8");
            $this->_conn == null;
            return true;
        }
        catch(PDOException $e)
        {
            echo "Connection failed: " . $e->getMessage();die;
        }
    }
    
    public function get($query, $one = false){
//         echo $query; echo '<hr>';
        $stmt = $this->_conn->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        if ($one){
            return $stmt->fetch();
        }else{
            return $stmt->fetchAll();
        }
    }
    public function run($query, $one = false){
//         echo $query; echo '<hr>';
        $stmt = $this->_conn->prepare($query);
        try {
            $stmt->execute();
            return array('status' => 'success', $this->_conn->lastInsertId());
        }catch (PDOException $e){
            //$this->log->log($e->getMessage(), 'database');
            $message = $e->getMessage();
            if ($e->getCode() == 23000){
                preg_match_all('/\'[a-zA-Z0-9]+\'/', $message, $error );
                return array('status' => 'error', 'message' =>$error);
            }
            return array('status' => 'error', 'message' => 'false');
        }
       
    }
    
    public function _update($data){
        if (is_array($data)){
            $cond = ' SET ';
            foreach ($data as $k => $v){
                $cond .= $k .' = '. $this->_escape($v) . ' , ';
            }
            $cond = substr($cond, 0, -2);
            return $cond;
        }
    }
    
    public function _insert($data){
        if (is_array($data)){
            $col = $val = ''; 
            foreach ($data as $k => $v){
                $col .= $k . ' , ';
                $val .= $this->_escape($v) . ' , ';
            }
            $col = substr($col, 0, -2);
            $val = substr($val, 0, -2);
            return ' ( ' . $col .' ) VALUES ( ' . $val . ' ) '; 
        }
    }
    
    public function _where($where, $type = 'AND'){
        $cond = '';
        if (! is_array($where)){
            $cond .= ' WHERE '.$this->_escape($where).' IS NOT NULL';
        }elseif (is_array($where)){
            $count = count($where);
            $cond .= ' WHERE ';
            foreach ($where as $k => $v){
                $count --;
                $cond .= $k .' = '.$this->_escape($v);
                if ($count != 0){
                    $cond .= ' ' .$type. ' ';
                }
            }
        }
        return $cond;
    }
    
    public function _order_by($item,$desc = false){
        $cond = ' ORDER BY '. $this->_escape($item) .' ';
        if ($desc){
            $cond .= ' DESC';
        }else{
            $cond .= ' ASC';
        }
        return $cond;
    }
    
    public function _escape($var){
        return $this->_conn->quote($var);
    }
    
    
}